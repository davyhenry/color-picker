var $ = require('jquery');
var colors = require('./colors');

$(function () {

	let socket = new WebSocket(`ws://${window.location.origin.replace('http://', '').replace('https://', '')}:8080`);
	var $colorsContainer = $('#colors');

	socket.onopen = function () {
		$('.connexion').css('display', 'none');
		var selectColor = function () {
			socket.send(colors[$(this).index() - 1].color);
		};

		$.each(colors, function (key, val) {
			$colorsContainer.append($("<div class='color'></div>").css("background-color", val.color));
		});
		$('.color').on('click', selectColor);
	};
});